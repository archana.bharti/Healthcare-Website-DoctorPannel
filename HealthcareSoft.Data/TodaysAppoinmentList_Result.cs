//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HealthcareSoft.Data
{
    using System;
    
    public partial class TodaysAppoinmentList_Result
    {
        public long Id { get; set; }
        public long AppointmentId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Age { get; set; }
        public string AppointmentDate { get; set; }
        public long PatientId { get; set; }
        public string ReasonForVisit { get; set; }
        public string AppointmentTime { get; set; }
        public string Illness { get; set; }
        public int QueueNumber { get; set; }
        public long Id1 { get; set; }
        public long DoctorId { get; set; }
    }
}
