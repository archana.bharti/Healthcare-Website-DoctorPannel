﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HealthcareSoft.Data.ViewModel
{
    public class DoctorPatientViewModel
    {
        public List<ReferPatientLModel> objReferPatientLModel = new List<ReferPatientLModel>();

        public List<ReferDoctorModel> objReferDoctorModel = new List<ReferDoctorModel>();
    }
}
