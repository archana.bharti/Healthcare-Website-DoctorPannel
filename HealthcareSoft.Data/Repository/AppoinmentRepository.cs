﻿using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Repository
{
   public class AppoinmentRepository: IAppoinmentRepository
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        public List<IllnessClass> GetIllnessList()
        {
             return _objHealthCareDBContext.Database.SqlQuery<IllnessClass>("SELECT Id, Discription FROM Symptoms UNION Select '0' as Id, 'Select All' as Name").ToList();
        }

      public AppoinmentDetailsModel GetAppointmentDetailbyPatientId(Int64? DoctorId,Int64? AppointmentId)
      {
            try
            {
                AppoinmentDetailsModel model = new AppoinmentDetailsModel();

                model = _objHealthCareDBContext.Database.SqlQuery<AppoinmentDetailsModel>("GetAppointmentDetailByPatientId @DoctorId=@DoctorId,@AppointmentId=@AppointmentId",
                                                                                           new SqlParameter("DoctorId", DoctorId),
                                                                                           new SqlParameter("AppointmentId", AppointmentId)).FirstOrDefault();


                return model;

            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public List<AllergiesClass> GetAllergieList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<AllergiesClass>("SELECT Id, Discription FROM Allergies UNION Select '0' as Id, 'Select Allergy' as Name").ToList();
        }

        public List<DiseaseClass> GetDiseaseList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<DiseaseClass>("SELECT Id, Name FROM Disease UNION Select '0' as Id, 'Select All' as Name order by Name asc").ToList();
        }

        public List<PillsClass> GetPillsName(string SearchName)
        {
            return _objHealthCareDBContext.Database.SqlQuery<PillsClass>("GetPillsReminder @SearchName=@SearchName",
               new SqlParameter("SearchName", SearchName)).ToList();
        }
        public List<DaysClass> GetdaysList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<DaysClass>("SELECT Id, NoOfDay FROM Days UNION Select '0' as Id, 'Select All' as NoOfDay").ToList();
        }
        public List<HoursClass> GetHoursList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<HoursClass>("SELECT Id, Hours FROM Hours UNION Select '0' as Id, 'Select All' as Hours").ToList();
        }

        public void AddAppointmentDetails(AppoinmentDetailsModel objAppoinmentDetailsModel,Int64 DoctorId)
        {
            try
            {
                //var CreatedBy = TempData["DoctorId"];

                Int64 DepartmentId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select DepartmentId from HospitalDoctorMapping where DoctorId={0}", DoctorId).FirstOrDefault();

                Int64 PatientId = _objHealthCareDBContext.Database.SqlQuery<Int64>("GetPatientIdByAppintId @AppointmentId=@AppointmentId",
                  new SqlParameter("AppointmentId", objAppoinmentDetailsModel.AppointmentId)).FirstOrDefault();

                //Int64 SymptomId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select SymptomId from Appointment where Id={0}", objAppoinmentDetailsModel.AppointmentId).FirstOrDefault();

                // var pillsname = objAppoinmentDetailsModel.PillsName;                

                // int i = pillsname.IndexOf(',');

                // if (i >= 0) pillsname = pillsname.Substring(0,i);             

                // Int64 PillsId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select Id from Pills where Name={0}", pillsname).FirstOrDefault();

                //_objHealthCareDBContext.Database.ExecuteSqlCommand("AddAppointmentDetails @AppointmentId=@AppointmentId,@DiseaseId=@DiseaseId,@AllergieId=@AllergieId,@SymptomId=@SymptomId,@ClinicalNotes=@ClinicalNotes,@RevisitTime=@RevisitTime,@CreatedBy=@CreatedBy,@AllergyTypeId=@AllergyTypeId,@DepartmentId=@DepartmentId,@PrescribeTestId=@PrescribeTestId,@Days=@Days,@IntervalId=@IntervalId,@PillsId=@PillsId,@PatientId=@PatientId",
                //                                                         new SqlParameter("AppointmentId", (Object)objAppoinmentDetailsModel.AppointmentId ?? DBNull.Value),
                //                                                         new SqlParameter("DiseaseId", objAppoinmentDetailsModel.DiseaseId),
                //                                                         new SqlParameter("AllergieId", objAppoinmentDetailsModel.AllergieId),
                //                                                         new SqlParameter("SymptomId", objAppoinmentDetailsModel.IllnessId),
                //                                                         new SqlParameter("ClinicalNotes", objAppoinmentDetailsModel.ClinicalNotes),
                //                                                         new SqlParameter("RevisitTime", objAppoinmentDetailsModel.RevisitTime),
                //                                                         new SqlParameter("CreatedBy", DoctorId),
                //                                                         new SqlParameter("AllergyTypeId", objAppoinmentDetailsModel.AllergyTypeId),
                //                                                         new SqlParameter("DepartmentId", DepartmentId),
                //                                                         new SqlParameter("PrescribeTestId",objAppoinmentDetailsModel.PrescribeTestId),
                //                                                         new SqlParameter("Days", objAppoinmentDetailsModel.Days),
                //                                                         new SqlParameter("IntervalId", objAppoinmentDetailsModel.IntervalId),
                //                                                         new SqlParameter("PillsId", PillsId),
                //                                                         new SqlParameter("PatientId", PatientId));



                var pillsname = objAppoinmentDetailsModel.PillsName;

                int i = pillsname.IndexOf(',');

                if (i >= 0) pillsname = pillsname.Substring(0, i);

                Int64 PillsId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select Id from Pills where Brand={0}", pillsname).FirstOrDefault();

                _objHealthCareDBContext.Database.ExecuteSqlCommand("AddAppointmentDetails @AppointmentId=@AppointmentId,@DiseaseId=@DiseaseId,@AllergieId=@AllergieId,@SymptomId=@SymptomId,@ClinicalNotes=@ClinicalNotes,@RevisitTime=@RevisitTime,@CreatedBy=@CreatedBy,@AllergyTypeId=@AllergyTypeId,@DepartmentId=@DepartmentId,@PrescribeTestId=@PrescribeTestId,@Days=@Days,@IntervalId=@IntervalId,@PillsId=@PillsId,@PatientId=@PatientId",
                                                                         new SqlParameter("AppointmentId", (Object)objAppoinmentDetailsModel.AppointmentId ?? DBNull.Value),
                                                                         new SqlParameter("DiseaseId", objAppoinmentDetailsModel.DiseaseId),
                                                                         new SqlParameter("AllergieId", objAppoinmentDetailsModel.AllergieId),
                                                                         new SqlParameter("SymptomId", objAppoinmentDetailsModel.IllnessId),
                                                                         new SqlParameter("ClinicalNotes", objAppoinmentDetailsModel.ClinicalNotes),
                                                                         new SqlParameter("RevisitTime", objAppoinmentDetailsModel.RevisitTime),
                                                                         new SqlParameter("CreatedBy", DoctorId),
                                                                         new SqlParameter("AllergyTypeId", objAppoinmentDetailsModel.AllergyTypeId),
                                                                         new SqlParameter("DepartmentId", DepartmentId),
                                                                         new SqlParameter("PrescribeTestId", objAppoinmentDetailsModel.PrescribeTestId),
                                                                         new SqlParameter("Days", objAppoinmentDetailsModel.Days),
                                                                         new SqlParameter("IntervalId", objAppoinmentDetailsModel.IntervalId),
                                                                         new SqlParameter("PillsId", PillsId),
                                                                         new SqlParameter("PatientId", PatientId));


            }
            catch (Exception ex)
            {
                
            }
             
        }

        public List<IntervalClass> GetIntervalList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<IntervalClass>("SELECT Id, Interval FROM IntervalMaster UNION Select '0' as Id, 'Select Interval' as Interval").ToList();
        }
        public List<PrescribeTestClass> GetPrescribeTestList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<PrescribeTestClass>("SELECT Id, TestName, Condition FROM PrescribeTest").ToList();
        }

        public List<DepartmentClass> GetDepartmentList()
        {
            return _objHealthCareDBContext.Database.SqlQuery<DepartmentClass>("SELECT Id, Name FROM Departments UNION Select '0' as Id, 'Select Department' as Name").ToList();
        }
    }
}
