﻿using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Repository
{
    public class DashboardRepository: IDashboardRepository
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();
        public List<TodaysAppointmentModel> TodaysAppoinmentList(Int64 DoctorId)
        {
            try
            {

                
                DateTime g = DateTime.Now;
                string createddate = DateTime.Now.ToString("MM-dd-yyyy");
                //DateTime Temp = Convert.ToDateTime(createddate);
                //string appStartDate = Temp.ToString("yyyy-MM-dd hh:mm:ss");
                //string appEndDate = Temp.AddDays(1).ToString("yyyy-MM-dd hh:mm:ss");

                List<TodaysAppointmentModel> list = _objHealthCareDBContext.Database.SqlQuery<TodaysAppointmentModel>("TodaysAppoinmentList @DoctorId=@DoctorId,@AppointmentDate=@AppointmentDate",
                                                                                        new SqlParameter("DoctorId", (object)DoctorId ?? DBNull.Value),
                                                                                        new SqlParameter("AppointmentDate", (object)createddate ?? DBNull.Value)).ToList();
                //new SqlParameter("AppEndDate", (object)appEndDate ?? DBNull.Value)).ToList();
                return list;
                
            }
            
            catch(Exception ex)
            {
                return null;
            }

        }
    }
}
