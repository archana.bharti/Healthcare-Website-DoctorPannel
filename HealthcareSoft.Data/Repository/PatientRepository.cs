﻿using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Repository
{
    public class PatientRepository: IPatientRepository
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();
        public List<PatientModel> GetGenderFilter(string value)
        {
               var list = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("select Id,Gender from Userprofile where Gender=@Gender",
                   new SqlParameter("Gender",value)).ToList();
                return list;
           
        }

        public List<ReferDoctorModel> GetDoctorReferList(string HospitalName,string DepartmentName)
        {
            var list = _objHealthCareDBContext.Database.SqlQuery<ReferDoctorModel>("DoctorList @HospitalName=@HospitalName,@DepartmentName=@DepartmentName",
                                                                    new SqlParameter("HospitalName", (object)HospitalName ?? DBNull.Value),
                                                                    new SqlParameter("DepartmentName", (object)DepartmentName ?? DBNull.Value)).ToList();
            return list;
            
        }
        public List<ReferPatientLModel> GetPatientReferList(Int64 DoctorId,string PatientName,string DoctorName,string Disease)
        { 
            var list = _objHealthCareDBContext.Database.SqlQuery<ReferPatientLModel>("ListOfReferPatient @CreatedBy=@CreatedBy,@PatientName=@PatientName,@DoctorName=@DoctorName,@Disease=@Disease",
                                                                                      new SqlParameter("CreatedBy", DoctorId),
                                                                                      new SqlParameter("PatientName", (object)PatientName ?? DBNull.Value),
                                                                                      new SqlParameter("DoctorName", (object)DoctorName ?? DBNull.Value),
                                                                                      new SqlParameter("Disease", (object)Disease ?? DBNull.Value)).ToList();
            return list;
        }

        public void AddReferredPatient(ReferDoctorModel model,Int64 DoctorId, Int64 PatientId)
        {
            try
            {
                int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("SaveReferedPatient @PatientId=@PatientId,@DoctorId=@DoctorId,@CreatedBy=@CreatedBy,@ModifiedBy=@ModifiedBy",
                                                                                     new SqlParameter("PatientId", PatientId),
                                                                                     new SqlParameter("DoctorId", DoctorId),
                                                                                     new SqlParameter("CreatedBy", DoctorId),
                                                                                     new SqlParameter("ModifiedBy", DoctorId));
            }
            catch(Exception ex)
            {
                
            }

        }
    }
}
