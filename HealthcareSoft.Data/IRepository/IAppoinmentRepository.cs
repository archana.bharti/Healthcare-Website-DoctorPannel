﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.IRepository
{
    public interface IAppoinmentRepository
    {
        List<IllnessClass> GetIllnessList();
        AppoinmentDetailsModel GetAppointmentDetailbyPatientId(Int64? DoctorId, Int64? AppointmentId);
        List<AllergiesClass> GetAllergieList();
        List<DiseaseClass> GetDiseaseList();
        List<PillsClass> GetPillsName(string SearchName);
        List<DaysClass> GetdaysList();
        List<HoursClass> GetHoursList();
        void AddAppointmentDetails(AppoinmentDetailsModel objAppoinmentDetailsModel,Int64 DoctorId);
        List<IntervalClass> GetIntervalList();
        List<PrescribeTestClass> GetPrescribeTestList();
        List<DepartmentClass> GetDepartmentList();

    }
}
