﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.IRepository
{
    public interface IDashboardRepository
    {
        List<TodaysAppointmentModel> TodaysAppoinmentList(Int64 DoctorId);

    }
}
