﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.IRepository
{
    public interface IPatientRepository
    {
        List<PatientModel> GetGenderFilter(string value);
        List<ReferDoctorModel> GetDoctorReferList(string HospitalName, string DepartmentName);
        List<ReferPatientLModel> GetPatientReferList(Int64 DoctorId, string PatientName, string DoctorName, string Disease);
        void AddReferredPatient(ReferDoctorModel model, Int64 DoctorId, Int64 PatientId);
    }
}
