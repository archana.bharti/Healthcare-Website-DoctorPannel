﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
   public class AppointmentHistoryModel
    {
        public Int64 AppointmentId { get; set; }
        public Int64 DoctorId { get; set; }
        public Int64 PatientId { get; set; }
        public int QueueNumber { get; set; }
        public string ReasonForVisit { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string PatientSymptoms { get; set; }
        public string DiseaseName { get; set; }       
        public string SymptomName { get; set; }
        public string AllergyName { get; set; }
        public string AllergyType { get; set; }
        public int? RevisitTime { get; set; }
        public string ClinicalNotes { get; set; }
        public int? Duration { get; set; }
        public string Interval { get; set; }
        public string Medicine { get; set; }
        public Int64? PrescriptionId { get; set; }

        public List<AppointmentHistoryModel> MedicineList { get; set; }
    }
}
