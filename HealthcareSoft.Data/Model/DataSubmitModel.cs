﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class DataSubmitModel
    {
        public string Notification { get; set; }
        public string LastUpdated { get; set; }
    }
}
