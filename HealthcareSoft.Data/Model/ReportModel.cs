﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class ReportModel
    {
        public Int64 Id { get; set; }
        public Int64 AppointmentId { get; set; }
        public int ReportType { get; set; }
        public byte[] Data { get; set; }
        public string FileName { get; set; }

        public int? IsPrint { get; set; }
    }
}
