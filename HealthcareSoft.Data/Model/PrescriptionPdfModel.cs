﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class PrescriptionPdfModel
    {        
        public List<Response> Response { get; set; }       
    }

    public class TabletList
    {        
        public Int64 PrescriptionId { get; set; }
        public string TabletName { get; set; }
        public string Repetaion { get; set; }
        public string Power { get; set; }
        public int Days { get; set; }        
    }
    public class Response
    {
        public Int64 Id { get; set; }
        public Int64 DoctorId { get; set; }
        public string DoctorName { get; set; }
        public Int64 PatientId { get; set; }
        public string PatientName { get; set; }
        public string DepartmentName { get; set; }
        public string QualificationName { get; set; }
        public string HospitalName { get; set; }
        public Int64 AppointmentId { get; set; }
        public int IsPrint { get; set; }
        public Int64 PrescriptionId { get; set; }
        public List<TabletList> TabletList { get; set; }
    }
}
