﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class MyScheduleModel
    {
        public Int64 Id { get; set; }
        public Int64 DoctorId { get; set; }
        public string Title { get; set; }
        public DateTime PresentDate { get; set; }
        public Boolean IsWorking { get; set; }
        public DateTime StartShift { get; set; }
        public DateTime EndShift { get; set; }

        public DateTime ScheduleDate { get; set; }
        public string ScheduleDay { get; set; }
        public DateTime ScheduleStartShift { get; set; }
        public DateTime ScheduleEndShift { get; set; }
        public string ScheduleMonth { get; set; }




        //public DateTime MondayDate { get; set; }
        //public Boolean IsMondayWorking { get; set; }
        //public DateTime MondayStartShift { get; set; }
        //public DateTime MondayEndShift { get; set; }

        //public DateTime TuesdayDate { get; set; }
        //public Boolean IsTuesdayWorking { get; set; }
        //public DateTime TuesdayStartShift { get; set; }
        //public DateTime TuesdayEndShift { get; set; }

        //public DateTime WednesdayDate { get; set; }
        //public Boolean IsWednesdayWorking { get; set; }
        //public DateTime WednesdayStartShift { get; set; }
        //public DateTime WednesdayEndShift { get; set; }

        //public DateTime ThursdayDate { get; set; }
        //public Boolean IsThursdayWorking { get; set; }
        //public DateTime ThursdayStartShift { get; set; }
        //public DateTime ThrusdayEndShift { get; set; }

        //public DateTime FridayDate { get; set; }
        //public Boolean IsFridayWorking { get; set; }
        //public DateTime FridayStartShift { get; set; }
        //public DateTime FridayEndShift { get; set; }

        //public DateTime SaturdayDate { get; set; }
        //public Boolean IsSaturdayWorking { get; set; }
        //public DateTime SaturdayStartShift { get; set; }
        //public DateTime SaturdayEndShift { get; set; }
    }

   public class ShowTimeModel
    {
        public Int64 Id { get; set; }
        public Int64 DoctorId { get; set; }
        public string Title { get; set; }
        public Boolean IsWorking { get; set; }
    
        public string start { get; set; }
       
        public string end { get; set; }
        public string value { get; set; }
        public TimeSpan ScheduleStartShift { get; set; }

        public TimeSpan ScheduleEndShift { get; set; }
        public string ScheduleMonth { get; set; }
    }
}
