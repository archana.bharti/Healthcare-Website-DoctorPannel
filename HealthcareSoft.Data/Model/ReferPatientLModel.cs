﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class ReferPatientLModel
    {
        public Int64 Id { get; set; }
        public Int64 PatientId { get; set; }
        public Int64 DoctorId { get; set; }
        public Int64 CreatedBy { get; set; }
        public string DocName { get; set; }
        public string PatientName { get; set; }
        public Int64? DiseaseId { get; set; }
        public string Disease { get; set; }
        public string ProfilePicture { get; set; }
        public string BloodGroup { get; set; }
        public string Gender { get; set; }
    }
}
