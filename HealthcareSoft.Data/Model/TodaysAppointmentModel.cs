﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
   public class TodaysAppointmentModel
    {
        public Int64 DoctorId { get; set; }
        public Int64 AppointmentId { get; set; }
        public string AppointmentDate { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int? Age { get; set; }
        public string AppointmentTime { get; set; }
        public int QueueNumber { get; set; }
        public Int64 PatientId { get; set; }
        public string ReasonForVisit { get; set; }
        public string Illness { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsCompleted { get; set; }
    }
}
