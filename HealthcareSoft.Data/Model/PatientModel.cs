﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HealthcareSoft.Data.Model
{
    public class PatientModel
    {       
        public Int64 AppointmentId { get; set; }
        public Int64 Id { get; set; }
        public Int64 DoctorId { get; set; }
        public string Name { get; set; }       
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string Discription { get; set; }
        public string ProfilePicture { get; set; }
    }

    public class Gender
    {
        public string GenderId { get; set; }
        public string GenderName { get; set; }
    }


}
