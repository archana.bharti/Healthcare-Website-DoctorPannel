﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HealthcareSoft.Data.Model
{
    public class IllnessClassA
    {

        public Int64 Id { get; set; }
        public string Discription { get; set; }
    }
  
    public class DepartmentClass
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }
    public class AllergyTypeClass
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }
    public class AllergiesClass
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
    }
    public class DiseaseClass
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }

    public class PillsClass
    {
        public Int64 PillsId { get; set; }
        public string Name { get; set; }
        public string Power { get; set; }
        public string TabletSyrup { get; set; }
        public string Subgroup { get; set; }
    }

    public class DaysClass
    {
        public Int64 Id { get; set; }
        public string NoOfDay { get; set; }
    }

    public class HoursClass
    {
        public Int64 Id { get; set; }
        public string Hours { get; set; }
    }

    public class IntervalClass
    {
        public int Id { get; set; }
        public string Interval { get; set; }
    }

    public class PrescribeTestClass
    {
        public Int64 Id { get; set; }
        public string TestName { get; set; }
        public string Condition { get; set; }
    }

    public class PriviousIllness
    {
        public Int64 AppointmentDetailsId { get; set; }
        public string DiseaseName { get; set; }
        public Int64 AppointmentId { get; set; }
        public Int64 DiseaseId { get; set; }
    }
    public class AppoinmentDetailsModel
    {
        HealthCareDBContext _objHealthCareDBContext = null;
        public AppoinmentDetailsModel()
        {
            _objHealthCareDBContext = new HealthCareDBContext();
            AllergyTypeList = new List<SelectListItem>();
            DiseaseList = new List<SelectListItem>();
            IllnessList = new List<SelectListItem>();
        }


        public Int64 AppointmentId { get; set; }
        public Int64 DoctorId { get; set; }
        public string AppointmentDate { get; set; }
        public string ProfilePicture { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Decimal? Height { get; set; }
        public Decimal? weight { get; set; }
        public string BloodPressure { get; set; }
        public string Pulse { get; set; }
        public int? Age { get; set; }
        public string  AppointmentTime { get; set; }
        public int? QueueNumber { get; set; }
        public Int64 PatientId { get; set; }
        public string Gender { get; set; }      
            public Int64? DiseaseId { get; set; }
            public Int64 AllergieId { get; set; }        
            public Int64 SymptomId { get; set; }
            public string ClinicalNotes { get; set; }
            public int RevisitTime { get; set; }
            public Int64 CreatedBy { get; set; }
        public string ReasonForVisit { get; set; }
        public string Discription { get; set; }
        public string PillsName { get; set; }
        public string NoOfDay { get; set; }
        public Int64 DaysId { get; set; }        
        public Int64 HoursId { get; set; }
        public string Hours { get; set; }
        public int Days { get; set; }
        public int IntervalId { get; set; }
        public Int64 PillsId { get; set; }
        public string Interval { get; set; }
        public Int64 PrescribeTestId { get; set; }
        public Int64? AllergyTypeId { get; set; }    
        public string Illness { get; set; }
        public Int64? IllnessId { get; set; }
        public Int64 DepartmentId { get; set; }
        public List<DepartmentClass> DepartmentList { get; set; }
        public List<SelectListItem> IllnessList { get; set; }

        public List<SelectListItem> AllergyTypeList { get; set; }
        public List<AllergiesClass> AllergieList { get; set; }
        public List<SelectListItem> DiseaseList { get; set; }
        public List<DaysClass> DaysList { get; set; }
        public List<HoursClass> HoursList { get; set; }
        public List<IntervalClass> IntervalList { get; set; }
        public List<PrescribeTestClass> PrescribeTestList { get; set; }

        public List<PriviousIllness> PatientPriviousIllness { get; set; }

    }
}
