﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
   public class LoginModel
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage  = "the email address is required")]
        [EmailAddress(ErrorMessage = "invalid email id")]
        public string Email { get; set; }


       // [datatype(datatype.phonenumber)]
       // [display(name = "phone number")]
       // [stringlength(13, minimumlength = 10)]
       // //[required(errormessage = "phone number required!")]
       //// [regularexpression("^[0-9]{8}$", errormessage = "entered phone format is not valid.")]
       // public string phoneno { get; set; }


        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 6)]
        [Required()]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }

        public string FirstName { get; set; }
        public Int64 Id { get; set; }
        public string ProfilePicture { get; set; }
        public string SpecializationName { get; set; }
        public string HospitalName { get; set; }
        public string QualificationName { get; set; }
        public string DepartmentName { get; set; }
    }

    public class ForgetPasswordModel

    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }
    }
    public class ResetPasswordModel

    {
        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 6)]
        [Required()]
        public string Password { get; set; }

        [NotMapped] // Does not effect with your database
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    public class EditProfileModel
    {

    }
}
