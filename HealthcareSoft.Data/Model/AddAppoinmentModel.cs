﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HealthcareSoft.Data.Model
{


    public class IllnessClass
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
    }

  
    public class AddAppoinmentModel
    {

        HealthCareDBContext _objHealthCareDBContext = null;
        public AddAppoinmentModel()
        {
            _objHealthCareDBContext = new HealthCareDBContext();
        }


        public Int64 DoctorId { get; set; }
        public string PatientName { get; set; }

       
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [StringLength(13, MinimumLength  = 10)]
        [Required(ErrorMessage = "Phone number required!")]
        public string PhoneNo { get; set; }
        public string Address { get; set; }

        [Required]
        public string ReasonForVisit { get; set; }
        public DateTime Selectdate { get; set; }
        public DateTime SelectTime { get; set; }
        public string Illness { get; set; }
        public Int64 SymptomId { get; set; }
        public Int64? IllnessId { get; set; }
        public List<IllnessClass> IllnessList { get; set; }
      

    }



}
