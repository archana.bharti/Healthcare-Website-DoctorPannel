﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class ReferDoctorModel
    {
        public Int64 DoctorId { get; set; }
        public Int64 PatientId { get; set; }
        public Int64 DepartmentId { get; set; }
        public String Name { get; set; }
        public String QualificationName { get; set; }
        public String DeptName { get; set; }
        public int? Age { get; set; }
        public String Gender { get; set; }
        public String Address { get; set; }
        public string ProfilePicture { get; set; }


    }
}
