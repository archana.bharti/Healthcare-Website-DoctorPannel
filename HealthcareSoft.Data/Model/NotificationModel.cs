﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class NotificationModel
    {
       public Int64 Id { get; set; }
       public Int64 PatientId { get; set; }
       public string PatientName { get; set; }
       public string ReasonForVisit { get; set; }
       public string AppointmentDate { get; set; }
       public string AppointmentTime { get; set; }

        public Int64 DoctorId { get; set; }
        public string ProfilePicture { get; set; }
        public Boolean? IsActive { get; set; }

        public Boolean? IsCompleted { get; set; }

    }
}
