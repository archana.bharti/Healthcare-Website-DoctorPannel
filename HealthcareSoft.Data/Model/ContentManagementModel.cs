﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareSoft.Data.Model
{
    public class ContentManagementModel
    {
        public Int64 Id { get; set; }
        public Int64 SliderNo { get; set; }
        public string Heading { get; set; }
        public string SubHeading { get; set; }
        public string Content { get; set; }
    }
}
