﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HealthcareSoft.Web.Startup))]
namespace HealthcareSoft.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
