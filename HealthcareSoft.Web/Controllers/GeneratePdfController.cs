﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    public class GeneratePdfController : PdfViewController
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        #region Action method for display & print Prescription Slip
        [AllowAnonymous]
        public ActionResult GeneratePrescriptionPdf(Int64 AppointmentId, int? IsPrint = 0)
        {
            PrescriptionPdfModel objPrescriptionPdf = new PrescriptionPdfModel();
            try
            {
                //var appointId = TempData["appID"];
                //TempData.Keep("appID");
                //string data = TempData.Peek("appID").ToString();

                Int64 AppointmentDetailId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select Id from AppointmentDetails where AppointmentId={0}", AppointmentId).FirstOrDefault();

                List<Response> model = new List<Response>();
                var li = _objHealthCareDBContext.Database.SqlQuery<Response>("GetPrescriptionByAppointmentId @AppointmentDetailId=@AppointmentDetailId",
                                                                             new SqlParameter("AppointmentDetailId", AppointmentDetailId)).FirstOrDefault();
                if (li != null)
                {

                    model.Add(li);

                    foreach (var item in model)
                    {
                        List<TabletList> objTabletList = _objHealthCareDBContext.Database.SqlQuery<TabletList>("GetTabletListByPresId @PrescriptionId = @PrescriptionId",
                                                     new SqlParameter("PrescriptionId", item.PrescriptionId)).ToList();
                        item.TabletList = objTabletList;

                    }


                    if (model != null)
                    {

                        return this.ViewPdf("", "GeneratePrescriptionPdf", model);

                    }
                    else
                    {
                        //ViewData["message"] = "";
                        return View();
                    }
                }
                else
                {
                    return this.ViewPdf("", "GeneratePrescriptionPdf", model);
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }
        #endregion

    }
}
