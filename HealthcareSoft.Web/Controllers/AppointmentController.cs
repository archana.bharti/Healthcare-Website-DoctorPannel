﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Data.Repository;
using HealthcareSoft.Web.CustomFilters;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web.UI;

namespace HealthcareSoft.Web.Controllers
{
    [CheckAuthentication]
    public class AppointmentController : Controller
    {
        IAppoinmentRepository _IAppoinmentRepository;
        public AppointmentController()
        {
            _IAppoinmentRepository = new AppoinmentRepository();
        }
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        public JsonResult GetDetailByPhone(string PhoneNo)
        {
            List<AddAppoinmentModel> list = new List<AddAppoinmentModel>();
            try
            {

                list = _objHealthCareDBContext.Database.SqlQuery<AddAppoinmentModel>("GetAppointmentDetailByPhoneno @PhoneNo=@PhoneNo",
                                                                                        new SqlParameter("PhoneNo", PhoneNo)).ToList();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAppointment()

        {
          
            AddAppoinmentModel addappoinmentmodel = new AddAppoinmentModel();
      
            //addappoinmentmodel.Selectdate = System.DateTime.Now;
            //addappoinmentmodel.IllnessList = _IAppoinmentRepository.GetIllnessList();
            //return View(addappoinmentmodel);
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> AddAppointment(AddAppoinmentModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   
                    model.DoctorId = ApplicationSession.UserId;

                    Int64 patientId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select Id from UserProfile where PhoneNo=@PhoneNo",
                                                                                    new SqlParameter("PhoneNo", model.PhoneNo)).FirstOrDefault();
                    if (patientId == 0)
                    {
                        string fullName = model.PatientName;
                        var names = fullName.Split(' ');
                        string FirstName = names[0];
                        string LastName = names[1];
                        string Password = CryptorEngine.Encrypt("12345678", true);

                        int roweffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("AddAppointmentWithNewPatient @FirstName=@FirstName,@LastName=@LastName,@Email=@Email,@Password=@Password,@Address=@Address,@PhoneNo=@PhoneNo,@DoctorId=@DoctorId,@ReasonForVisit=@ReasonForVisit,@AppointmentDate=@AppointmentDate,@AppointmentTime=@AppointmentTime,@Illness=@Illness",
                                                                                               new SqlParameter("FirstName", FirstName),
                                                                                               new SqlParameter("LastName", LastName),
                                                                                               new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                                                                                               new SqlParameter("Password", Password),
                                                                                               new SqlParameter("Address", model.Address),
                                                                                               new SqlParameter("PhoneNo", model.PhoneNo),
                                                                                               new SqlParameter("DoctorId", model.DoctorId),
                                                                                               new SqlParameter("ReasonForVisit", model.ReasonForVisit),
                                                                                               new SqlParameter("AppointmentDate", model.Selectdate),
                                                                                               new SqlParameter("AppointmentTime", model.SelectTime),
                                                                                               new SqlParameter("Illness", model.Illness));
                            if (roweffected > 0)
                            {
                                if (model.Email != null)
                                {
                                    var message = new MailMessage();
                                    message.To.Add(new MailAddress(model.Email));  //replace with valid value
                                    message.From = new MailAddress("testifiedemail@gmail.com");  //replace with valid value
                                    message.Subject = "Alert! Stetho Registration";
                                    message.Body = "Please register to the Stetho mobile application";


                                    message.IsBodyHtml = true;

                                    using (var smtp = new SmtpClient())
                                    {
                                        var credential = new NetworkCredential
                                        {
                                            UserName = "testifiedemail@gmail.com",  //replace with valid value
                                            Password = "testifiedpassword@hackfree" //replace with valid value
                                        };
                                        smtp.Credentials = credential;
                                        smtp.Host = "smtp.gmail.com";
                                        smtp.Port = 587;
                                        smtp.EnableSsl = true;
                                        await smtp.SendMailAsync(message);
                                    }
                                    ViewBag.Message = "Link has been send successfully";
                                }
                            TempData["Message"] = "Appointment Added Successfully!";
                            TempData.Keep();
                            return RedirectToAction("Index", "Dashboard");
                        }
                        
                    }
                    else
                    {
                        int roweffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("AddAppointment @ReasonForVisit=@ReasonForVisit,@AppointmentDate=@AppointmentDate,@AppointmentTime=@AppointmentTime,@DoctorId=@DoctorId,@PatientId=@PatientId,@Illness=@Illness",
                                                                                           new SqlParameter("ReasonForVisit", model.ReasonForVisit),
                                                                                           new SqlParameter("AppointmentDate", model.Selectdate),
                                                                                           new SqlParameter("AppointmentTime", model.SelectTime),
                                                                                           new SqlParameter("DoctorId", model.DoctorId),
                                                                                           new SqlParameter("PatientId", patientId),
                                                                                           new SqlParameter("Illness", model.Illness));
                        if (roweffected > 0)
                        {
                            TempData["Message"] = "Appointment Added Successfully!";
                            TempData.Keep();
                            return RedirectToAction("Index", "Dashboard");
                        }                       
                       
                    }
                   
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }                
            }
            return View();
        }


       [HttpPost]
        public JsonResult GetPatientName(string PatientName)
       {
            List<AddAppoinmentModel> list = new List<AddAppoinmentModel>();
            try
            {                
                list = _objHealthCareDBContext.Database.SqlQuery<AddAppoinmentModel>("GetPatientname @Name=@Name",
                                                                                    new SqlParameter("Name", PatientName)).ToList();
             }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
           
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AppoinmentDetails(Int64? DoctorId, Int64? AppointmentId)
        {            
            try
            {
               

                Int64 PatientId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select PatientId from Appointment where Id={0}", AppointmentId).FirstOrDefault();

                List<PriviousIllness> listOfPriviousIllness = _objHealthCareDBContext.Database.SqlQuery<PriviousIllness>("PriviousIllness @PatientId=@PatientId",
                                                                new SqlParameter("PatientId", PatientId)).ToList();


                if (TempData["Success"] != null)
                {
                    ViewBag.Success = TempData["Success"].ToString();
                }

                TempData["appID"] = AppointmentId;
                var FileAppId = TempData["appIDFile"];
                AppoinmentDetailsModel model = new AppoinmentDetailsModel();
                model.AppointmentId = AppointmentId.Value;
                model.DoctorId = ApplicationSession.UserId;
                AppoinmentDetailsModel GetDetails = _IAppoinmentRepository.GetAppointmentDetailbyPatientId(model.DoctorId, AppointmentId.Value);


                if (GetDetails.ProfilePicture==null)
                {
                    GetDetails.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                }
                else if(GetDetails.ProfilePicture!=null)
                {
                    GetDetails.ProfilePicture = "http://18.219.253.188:8081" + GetDetails.ProfilePicture;
                }
                else
                {
                    GetDetails.ProfilePicture = "http://18.219.253.188:8081" + GetDetails.ProfilePicture;
                }

                //GetDetails.IllnessList = _IAppoinmentRepository.GetIllnessList();
                GetDetails.AllergieList = _IAppoinmentRepository.GetAllergieList();
                GetDetails.DepartmentList = _IAppoinmentRepository.GetDepartmentList();
                GetDetails.IntervalList = _IAppoinmentRepository.GetIntervalList();
                GetDetails.PrescribeTestList = _IAppoinmentRepository.GetPrescribeTestList();

                GetDetails.PatientPriviousIllness = listOfPriviousIllness;

                var list = _IAppoinmentRepository.GetPrescribeTestList();
                var prescriptionList = list.Select(s => new
                {
                    Id = s.Id,
                    Details = string.Format("{0} || {1}", s.TestName, s.Condition)
                })
                .ToList();
                ViewBag.PrescribeList = prescriptionList;
                return View(GetDetails);
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }           
        }

        [HttpPost]
        public JsonResult UpdateDemographicById(string[] Categories)
        {
            int result;
            Int64 Id = (Int64)TempData["appID"];
            AppoinmentDetailsModel obj = new AppoinmentDetailsModel();
            try
            {
                decimal Height = Convert.ToDecimal(Categories[0]);
                decimal Weight = Convert.ToDecimal(Categories[1]);
                string bp = Categories[2];
                string pulse = Categories[3];



                Int64 PatientId = _objHealthCareDBContext.Database.SqlQuery<Int64>("GetPatientIdByAppintId @AppointmentId=@AppointmentId",
                                                                                  new SqlParameter("AppointmentId", Id)).FirstOrDefault();

                result = _objHealthCareDBContext.Database.ExecuteSqlCommand("Update UserProfile set Height =" + Height + "," + "Weight=" + Weight + "," + "BloodPressure=" + "'" + bp + "'" + "," + "Pulse=" + "'" + pulse + "'" + " " + "where Id=" + PatientId);



            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(new { Url = "AppointmentDetails?DoctorId=0&AppointmentId=" + Id });
        }

        [HttpPost]
        public ActionResult AppoinmentDetails(AppoinmentDetailsModel objAppoinmentDetailsModel)
        {
            try
            {
                if(objAppoinmentDetailsModel.AppointmentId!=0)
                {
                    
                }
                // TempData["DoctorId"] = ApplicationSession.UserId;  
                var DoctorId = ApplicationSession.UserId;  
              _IAppoinmentRepository.AddAppointmentDetails(objAppoinmentDetailsModel, DoctorId);
                
                if(objAppoinmentDetailsModel!=null)
                {
                    TempData["Success"] = "Prescription Added Successfully!";
                    TempData.Keep();
                    return RedirectToAction("Index", "Dashboard");
                }
              
                //return RedirectToAction("Index", "Dashboard");
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
            return View();
        }

   


        [HttpPost]
        public ActionResult GetPillsName(string SearchName)
        {
            var pillName = _IAppoinmentRepository.GetPillsName(SearchName);
            List<PillsClass> list = pillName;
            var pillsList = list.Select(s => new
            {
                s.PillsId,
                Details = string.Format("{0} , {1} , {2} & {3}", s.Name,s.Subgroup ,s.TabletSyrup, s.Power)
                
            })
            .ToList();
            //ViewBag.PillsList = pillsList;
            return Json(data: pillsList, behavior: JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllergyTypeByAllergyId(Int64 AllergyId)
        {
            try
            {
                var allergyType = _objHealthCareDBContext.Database.SqlQuery<AllergyTypeClass>("select * from AllergyType where AllergyId=@AllergyId", new SqlParameter("AllergyId", AllergyId)).ToList();
                var result = (from a in allergyType
                              select new
                              {
                                  id = a.Id,
                                  name = a.Name
                              }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Invalid Request", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetDiseaseNameByDepartmentId()
        {
            try
            {
                var DoctorId = ApplicationSession.UserId;
                Int64 DepartmentId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select DepartmentId from HospitalDoctorMapping where DoctorId={0}", DoctorId).FirstOrDefault();

                var DiseaseName = _objHealthCareDBContext.Database.SqlQuery<DiseaseClass>("select * from Disease where DepartmentId=" + DepartmentId + "order by Name asc").ToList();
                var result = (from a in DiseaseName
                              select new
                              {
                                  id = a.Id,
                                  name = a.Name
                              }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Invalid Request", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetIllnesssNameByDiseaseId(Int64 DiseaseId)
        {
            try
            {
                var IllnessName = _objHealthCareDBContext.Database.SqlQuery<IllnessClassA>("select * from Symptoms where DiseaseId=@DiseaseId", new SqlParameter("DiseaseId", @DiseaseId)).ToList();
                var result = (from a in IllnessName
                              select new
                              {
                                  id = a.Id,
                                  name = a.Discription 
                              }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Invalid Request", JsonRequestBehavior.AllowGet);
            }
        }
        

        [HttpGet]
        public ActionResult AppointmentHistoryByPatientId(Int64 AppointmentId)
        {
            try
            {
                Int64 DoctorId = ApplicationSession.UserId;
                AppointmentHistoryModel model = new AppointmentHistoryModel();

                model = _objHealthCareDBContext.Database.SqlQuery<AppointmentHistoryModel>("GetAppointmentHistoryByPatientId @AppointmentId=@AppointmentId,@DoctorId=@DoctorId",
                                                                                                new SqlParameter("AppointmentId", AppointmentId),
                                                                                                new SqlParameter("DoctorId", @DoctorId)).FirstOrDefault();

                List<AppointmentHistoryModel> MedicineList = new List<AppointmentHistoryModel>();
                MedicineList = _objHealthCareDBContext.Database.SqlQuery<AppointmentHistoryModel>("GetPillHistoryForPatient @AppointmentId=@AppointmentId",
                                                                                                    new SqlParameter("AppointmentId", AppointmentId)).ToList();

                if(MedicineList.Count> 0)
                {
                    model.MedicineList = MedicineList;
                    return View(model);
                }
                else
                {
                    return View(model);
                }
               
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            
        }


    }
}
