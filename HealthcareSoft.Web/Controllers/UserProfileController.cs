﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Web.CustomFilters;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace HealthcareSoft.Web.Controllers
{

    public class UserProfileController : Controller
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();
        //string ChangePasswordUrl = WebConfigurationManager.AppSettings["HCLocal"];
        string ChangePasswordUrl = WebConfigurationManager.AppSettings["HCOnline"];
        HttpCookie cookie = new HttpCookie("Login");



        public ActionResult Login()
        {

            if (TempData["NotValid"] != null)
            {
                ViewBag.NotValid = TempData["NotValid"].ToString();
            }
            LoginModel model = new LoginModel();
            if (Request.Cookies["Login"] != null)
            {
                model.Email = Request.Cookies["Login"].Values["Email"];
                model.Password = Request.Cookies["Login"].Values["Password"];
                model.RememberMe = Convert.ToBoolean(Request.Cookies["Login"].Values["RememberMe"]);
            }
            return View(model);            
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {

            var rem = model.RememberMe;
            if (ModelState.IsValid)
            { 
                try
                {
                   if(model.Email != null)
                    {                      
                        model.Password = CryptorEngine.Encrypt(model.Password,true);
                       model = _objHealthCareDBContext.Database.SqlQuery<LoginModel>("LoginDoc @Email=@Email,@Password=@Password",
                                                                            new SqlParameter("Email", model.Email),
                                                                            new SqlParameter("Password", model.Password)).FirstOrDefault();

                        if(model!=null)
                        {
                            if (rem)
                            {
                                model.RememberMe = true;
                            }
                            FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
                            if (model.RememberMe)
                            {
                                cookie.Values.Add("Email", model.Email);
                                cookie.Values.Add("Password", CryptorEngine.Decrypt(model.Password, true));
                                cookie.Values.Add("Rememberme", (model.RememberMe).ToString());
                                cookie.Expires = DateTime.Now.AddDays(30);
                                Response.Cookies.Add(cookie);

                            }
                            else
                            {
                                Response.Cookies["Login"].Values["Rememberme"] = "false";
                            }
                            ApplicationSession.Login(model);
                            Session["Id"] = model.Id;
                            Session["username"] = model.FirstName;
                            Session["ProfilePicture"] = model.ProfilePicture;
                            Session["HospitalName"] = model.HospitalName;
                            Session["rememberme"] = model.RememberMe;
                            return RedirectToAction("Index", "Dashboard");
                        }
                        else
                        {
                            TempData["NotValid"] = "Invalid User Name OR Password ..";
                            TempData.Keep();
                            return RedirectToAction("Login", "UserProfile");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please fill up right information!!");
                        return View();
                    }
            } 
            catch (Exception ex)
            {
                    return Json(ex.Message);
             }
        }
            return View(model);
        }

        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgetPassword(ForgetPasswordModel model, string email)
        {
            if (ModelState.IsValid)
            {


                Int64 Id = _objHealthCareDBContext.Database.SqlQuery<Int64>("Select Id from dbo.UserProfile where Email = @Email", new SqlParameter("@Email", email)).FirstOrDefault();

                if (Id!= null)
                {
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(email));  //replace with valid value
                    message.From = new MailAddress("testifiedemail@gmail.com");  //replace with valid value
                    message.Subject = "Click Here To Change Password!";
                    // message.Body = "Please Click on Link To Change Password<br/>" + ChangePasswordUrl + EmployeeId;
                    message.Body = "Please Click on Link To Change Password<br/> <a href = '" + ChangePasswordUrl + Id + "' > " + ChangePasswordUrl + Id + " </a>";


                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = "testifiedemail@gmail.com",  //replace with valid value
                            Password = "testifiedpassword@hackfree" //replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);
                    }
                    ViewBag.Message = "Link has been send successfully";
                }
            }
            else
            {
                ModelState.AddModelError("", "Invalid user name");
            }
            return View("ForgetPassword");
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(Int64? Id)
        {
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel objResetPasswordModel, Int64 Id)
        {
            try
            {
                ViewBag.Id = Id;
                if (ModelState.IsValid)
                {
                    objResetPasswordModel.Password = CryptorEngine.Encrypt(objResetPasswordModel.Password, true);
                    int effectedRecord = _objHealthCareDBContext.Database.ExecuteSqlCommand("update dbo.UserProfile set Password=@Password where Id=@Id ",
                                                                                            new SqlParameter("Id", Id),
                                                                                            new SqlParameter("Password", objResetPasswordModel.Password));
                    if (effectedRecord > 0)
                    {

                        string EmailId = _objHealthCareDBContext.Database.SqlQuery<string>("Select Email from dbo.UserProfile Where Id=@Id", 
                                                                                            new SqlParameter("Id", Id)).FirstOrDefault();

                        var message = new MailMessage();
                        message.To.Add(new MailAddress(EmailId));  //replace with valid value
                        message.From = new MailAddress("testifiedemail@gmail.com");  //replace with valid value
                        message.Subject = "Password Reset!";
                        message.Body = "Your New Password Is<br/>" + CryptorEngine.Decrypt(objResetPasswordModel.Password,true);
                        message.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {
                            var credential = new NetworkCredential
                            {
                                UserName = "testifiedemail@gmail.com",  // replace with valid value
                                Password = "testifiedpassword@hackfree"  // replace with valid value
                            };
                            smtp.Credentials = credential;
                            smtp.Host = "smtp.gmail.com";                           
                            smtp.Port = 587;
                            smtp.EnableSsl = true;
                            await smtp.SendMailAsync(message);
                        }
                        ViewBag.Message = "Password has been sent to your EmailId";
                    }
                    else
                    {
                        return View();
                    }
                }
               
                return View();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }


        }

        [HttpGet]
        public ActionResult EditProfile()
        {
            try
            {
                if (TempData["Edit"] != null)
                {
                    ViewBag.Edit = TempData["Edit"].ToString();
                }

                DoctorEditProfileModel model = new DoctorEditProfileModel();
                var value = ApplicationSession.UserId;
                model = _objHealthCareDBContext.Database.SqlQuery<DoctorEditProfileModel>("EditDoctorProfile @DoctorId=@DoctorId",
                                                            new SqlParameter("DoctorId", value)).FirstOrDefault();
                return View(model);
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
           
        }

        [HttpPost]
        public ActionResult EditProfile(DoctorEditProfileModel model)
        {
            try
            {
                var value = ApplicationSession.UserId;
                int roweffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("UpdateDoctorProfile @DoctorId=@DoctorId,@FirstName=@FirstName,@MiddelName=@MiddelName,@LastName=@LastName,@Address=@Address,@PostalCode=@PostalCode,@PhoneNo=@PhoneNo",
                                                                                      new SqlParameter("DoctorId", value),
                                                                                      new SqlParameter("FirstName", model.FirstName),
                                                                                      new SqlParameter("MiddelName", (object)model.MiddelName ?? DBNull.Value),
                                                                                      new SqlParameter("LastName", model.LastName),
                                                                                      new SqlParameter("Address", model.Address),
                                                                                      new SqlParameter("PostalCode", model.PostalCode),
                                                                                      new SqlParameter("PhoneNo", model.PhoneNo));
                if(roweffected!=0)
                {
                    TempData["Edit"] = "Profile Updated Successfully!";
                    TempData.Keep();
                    return RedirectToAction("EditProfile", "UserProfile");

                }
                else
                {
                    TempData["Edit"] = "Some Error occurred";
                    TempData.Keep();
                    return RedirectToAction("EditProfile", "UserProfile");
                }
               
                //return RedirectToAction("Index", "Dashboard");
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
            return View();
        }


        public ActionResult ChangePassword()
        {
            
            return View();
        }

      
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
               
                Int64 UserId = ApplicationSession.UserId;
                model.NewPassword = CryptorEngine.Encrypt(model.NewPassword, true);

                int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("Update UserProfile set Password = @Password where Id = @UserId",
                                                                                    new SqlParameter("Password", model.NewPassword),
                                                                                    new SqlParameter("UserId", UserId));

                
            }
           else
            {
                ModelState.AddModelError("","model is not valid");
            }
            return RedirectToAction("EditProfile", "UserProfile");
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            ApplicationSession.Logout();
            return RedirectToAction("Login");



        }
    }
}
