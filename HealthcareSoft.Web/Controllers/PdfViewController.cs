﻿using HealthcareSoft.Web.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;

namespace HealthcareSoft.Web.Controllers
{
    public class PdfViewController : Controller
    {

        private readonly HtmlViewRenderer htmlViewRenderer;
        private readonly StandardPdfRenderer standardPdfRenderer;

        public PdfViewController()
        {
            this.htmlViewRenderer = new HtmlViewRenderer();
            this.standardPdfRenderer = new StandardPdfRenderer();
        }

        protected ActionResult ViewPdf(string pageTitle, string viewName, object model)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {

                string htmlText = this.htmlViewRenderer.RenderViewToString(this, viewName, model);
                StringReader sr = new StringReader(htmlText);
                Document pdfDoc = new Document(PageSize.A4, 100f, 100f, 100f, 100f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                string imageURL = Server.MapPath("~/Content/ProfileImages/Rx_logo.png");
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
                //Resize image depend upon your need
                jpg.ScaleToFit(45f, 45f);                
                jpg.SpacingBefore = 10f;                
                jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.ALIGN_LEFT;
                pdfDoc.Add(jpg);
                string centerimgeURL = Server.MapPath("~/Content/ProfileImages/center.jpg");
                iTextSharp.text.Image c_jpg = iTextSharp.text.Image.GetInstance(centerimgeURL);
                c_jpg.SetAbsolutePosition((PageSize.A4.Width - c_jpg.ScaledWidth) / 2, (PageSize.A4.Height - c_jpg.ScaledHeight) / 2);
                pdfDoc.Add(c_jpg);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);                
                pdfDoc.Close();                
                return File(stream.ToArray(), "application/pdf");
            }
            
            // Render the view html to a string.
            //string htmlText = this.htmlViewRenderer.RenderViewToString(this, viewName, model);

            //// Let the html be rendered into a PDF document through iTextSharp.
            //byte[] buffer = standardPdfRenderer.Render(htmlText, pageTitle);

            //// Return the PDF as a binary stream to the client.
            //return new BinaryContentResult(buffer, "application/pdf");
        }

        public byte[] GetPdfBuffer(Controller controller, string pageTitle, string viewName, object model)
        {
            // Render the view html to a string.
            string htmlText = this.htmlViewRenderer.RenderViewToString(this, viewName, model);

            // Let the html be rendered into a PDF document through iTextSharp.
            byte[] buffer = standardPdfRenderer.Render(htmlText, pageTitle);

            return buffer;
        }

    }
}
