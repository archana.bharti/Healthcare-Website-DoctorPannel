﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Web.CustomFilters;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{

    //[CheckAuthentication]
    [RoutePrefix("Pdf")]
    public class PrintReportController : PdfViewController
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        [AllowAnonymous]
        public ActionResult GetReportForPatient(Int64 appointmentId)
        {
            try
            { 

                ReportModel obj = _objHealthCareDBContext.Database.SqlQuery<ReportModel>("GetReport @AppointmentId=@AppointmentId",
                                                                                           new SqlParameter("AppointmentId", appointmentId)).FirstOrDefault();


                string imreBase64Data = Convert.ToBase64String(obj.Data);
                byte[] contents = (byte[])obj.Data;
                return File(contents, "filename.pdf");
            }
           catch(Exception ex)
            {
                return null;
            }
          
        }


        [AllowAnonymous]
        public ActionResult GetClinicalReportForPatient(Int64 appointmentId)
        {
            try
            {
                //var appointId = TempData["appID"];
                //TempData.Keep("appID");
                //string data = TempData.Peek("appID").ToString();
                ReportModel obj = _objHealthCareDBContext.Database.SqlQuery<ReportModel>("GetClinicalReport @AppointmentId=@AppointmentId",
                                                                                           new SqlParameter("AppointmentId", appointmentId)).FirstOrDefault();


                string imreBase64Data = Convert.ToBase64String(obj.Data);
                byte[] contents = (byte[])obj.Data;
                return File(contents, "application/pdf");
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
