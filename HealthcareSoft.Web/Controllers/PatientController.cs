﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Data.Repository;
using HealthcareSoft.Data.ViewModel;
using HealthcareSoft.Web.CustomFilters;
using HealthcareSoft.Web.Enum;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    [CheckAuthentication]

    public class PatientController : Controller
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        IPatientRepository _IPatientRepository;
        public PatientController()
        {
            _IPatientRepository = new PatientRepository();
        }

        public ActionResult ViewPatient()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PatientList(string name, string value)
        {
            try
            {              
                List<PatientModel> list1 = new List<PatientModel>();              
                var value1 = ApplicationSession.UserId;
                if(name==null && value==null)
                {
                    list1 = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("PatientList @DoctorId=@DoctorId",
                                                                   new SqlParameter("DoctorId", value1)).ToList();

                    foreach(var item in list1)
                    {
                        if(item.ProfilePicture==null)
                        {
                            item.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                        }
                        else if(item.ProfilePicture!=null)
                        {
                            item.ProfilePicture= "http://18.219.253.188:8081" + item.ProfilePicture;
                        }
                        else
                        {
                            item.ProfilePicture = "http://18.219.253.188:8082" + item.ProfilePicture;
                        }
                    }
                    

                    return View(list1);
                }               
               
                return View();
            }
            catch(Exception ex)
            {
                return View();
            }           
        }


        [HttpPost]
        public ActionResult PatientList(PatientModel model,string value)
        {
            try
            {           
                var value1 = ApplicationSession.UserId;
                if(model.Name!=null && value == "")
                {
                    List<PatientModel> list1 = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("PatientList @DoctorId=@DoctorId,@Name=@Name",
                                                                   new SqlParameter("DoctorId", value1),
                                                                   new SqlParameter("Name", (object)model.Name ?? DBNull.Value)).ToList();
                    return View(list1);
                }

                if (model.Name == null && value != null)
                {
                    List<PatientModel> list1 = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("FilterGender @DoctorId=@DoctorId,@Value=@Value",
                                                     new SqlParameter("DoctorId", value1),
                                                     new SqlParameter("Value", value)).ToList();
                    return View(list1);
                }
                if (model.Name!= null && value != "")
                {
                    List<PatientModel> list1 = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("PatientList @DoctorId=@DoctorId,@Name=@Name,@Value=@Value",
                                                     new SqlParameter("DoctorId", value1),
                                                     new SqlParameter("Name", (object)model.Name ?? DBNull.Value),
                                                     new SqlParameter("Value", value)).ToList();
                    return View(list1);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult ReferPatient(Int64? DoctorId, string HospitalName, string DepartmentName, string PatientName, string DoctorName, string Disease)
        {
            try
            {
                ReferPatientLModel model = new ReferPatientLModel();
                model.DoctorId = ApplicationSession.UserId;

                var referDoc = _IPatientRepository.GetDoctorReferList(HospitalName,DepartmentName);
                dynamic mymodel = new ExpandoObject();
                mymodel.objReferDoctorModel = _IPatientRepository.GetDoctorReferList(HospitalName, DepartmentName);
                mymodel.objReferPatientLModel = _IPatientRepository.GetPatientReferList(model.DoctorId,PatientName,DoctorName,Disease);

                return View(mymodel);
            }
            catch(Exception ex)
            {
                return null;
            }           
        }

        [HttpPost]
        public ActionResult ReferPatient(ReferDoctorModel model,Int64? DoctorId)
        {
            try
            {
                var a = TempData["appID"];
                Int64 PatientId = _objHealthCareDBContext.Database.SqlQuery<Int64>("select PatientId from Appointment where Id={0}", a).FirstOrDefault();

                model.DoctorId = ApplicationSession.UserId;
                _IPatientRepository.AddReferredPatient(model, model.DoctorId, PatientId);
                return RedirectToAction("ReferPatient", "Patient");
            }
            catch(Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            } 
          
        }



        [HttpPost]
        public ActionResult AddPatientReport(HttpPostedFileBase file)
        {
            try
            {
                var appointId = TempData["appID"];
                String FileExt = Path.GetExtension(file.FileName).ToUpper();

                if (FileExt == ".PDF")
                {
                    Stream str = file.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] FileDet = Br.ReadBytes((Int32)str.Length);

                    ReportModel Fd = new ReportModel();
                    Fd.AppointmentId = Convert.ToInt64(appointId);
                    Fd.ReportType = 1;
                    Fd.FileName = file.FileName;
                    Fd.Data = FileDet;
                    SaveFileDetails(Fd);


                    TempData["File"] = "File Uploaded Successfully!";
                    TempData.Keep();

                    TempData["appIDFile"] = appointId;
                    var AppointmentId = TempData["appID"];
                    //return RedirectToAction("AppoinmentDetails", "Appointment");
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {

                    ViewBag.FileStatus = "Invalid file format.";
                    return View();

                }

                return View();
            }
            catch(Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }


        private void SaveFileDetails(ReportModel objDet)
        {
            try
            {

                var appointId = TempData["appID"];
                int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("AddFileDetails @AppointmentId=@AppointmentId,@ReportType=@ReportType,@Data=@Data,@FileName=@FileName",
                                                                                        new SqlParameter("AppointmentId", appointId),
                                                                                        new SqlParameter("ReportType", 1),
                                                                                        new SqlParameter("Data", objDet.Data),
                                                                                        new SqlParameter("FileName", objDet.FileName));
            }
            catch(Exception ex)
            {
               
            }
        }


        [HttpPost]
        public ActionResult AddPatientClinicalReport(HttpPostedFileBase fileclinical)
        {
            try
            {
                var appointId = TempData["appID"];
                String FileExt = Path.GetExtension(fileclinical.FileName).ToUpper();

                if (FileExt == ".PDF")
                {
                    Stream str = fileclinical.InputStream;
                    BinaryReader Br = new BinaryReader(str);
                    Byte[] FileDet = Br.ReadBytes((Int32)str.Length);

                    ReportModel Fd = new ReportModel();
                    Fd.AppointmentId = Convert.ToInt64(appointId);
                    Fd.ReportType = 2;
                    Fd.FileName = fileclinical.FileName;
                    Fd.Data = FileDet;
                    SaveClinicalFile(Fd);

                    TempData["File"] = "File Uploaded Successfully!";
                    TempData.Keep();

                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {

                    ViewBag.FileStatus = "Invalid file format.";
                    return View();

                }

                return View();
            }
            catch (Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        private void SaveClinicalFile(ReportModel objReportModel)
        {
            try
            {

                var appointId = TempData["appID"];
                int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("SaveClinicalReport @AppointmentId=@AppointmentId,@ReportType=@ReportType,@Data=@Data,@FileName=@FileName",
                                                                                        new SqlParameter("AppointmentId", appointId),
                                                                                        new SqlParameter("ReportType", 2),
                                                                                        new SqlParameter("Data", objReportModel.Data),
                                                                                        new SqlParameter("FileName", objReportModel.FileName));
            }
            catch (Exception ex)
            {

            }
        }


        [HttpGet]
        public JsonResult FilterByGender(string gender)
        {
            try
            {
                PatientModel model =new PatientModel();

                model.DoctorId = ApplicationSession.UserId;
                List<PatientModel> list = _objHealthCareDBContext.Database.SqlQuery<PatientModel>("FilterGender @DoctorId=@DoctorId,@Value=@Value",
                                                    new SqlParameter("DoctorId", model.DoctorId),
                                                    new SqlParameter("Value", gender)).ToList();

                //return Json(list);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}