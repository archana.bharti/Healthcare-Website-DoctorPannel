﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    public class MyScheduleController : Controller
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        [HttpGet]    
        public ActionResult Index()
        {
            try
            {

            }
            catch(Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(MyScheduleModel objMyScheduleModel, string type, int? value)
        {
            try
            {
                objMyScheduleModel.DoctorId = ApplicationSession.UserId;
                DateTime dt = objMyScheduleModel.PresentDate;
                DayOfWeek dow = dt.DayOfWeek; //enum
                type = dow.ToString(); //string

                String sDate = objMyScheduleModel.PresentDate.ToString();
                DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

                String dy = datevalue.Day.ToString();
                string month = datevalue.ToString("MMMM");
                String mn = datevalue.Month.ToString(datevalue.Month.ToString());
                String yy = datevalue.Year.ToString();

                if (objMyScheduleModel.IsWorking == true)
                {
                    if (type != null)
                    {
                        int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("SaveDoctorsTimeSchedule @DoctorId=@DoctorId,@ScheduleDate=@ScheduleDate,@ScheduleDay=@ScheduleDay,@ScheduleStartShift=@ScheduleStartShift,@ScheduleEndShift=@ScheduleEndShift,@IsWorking=@IsWorking,@ScheduleMonth=@ScheduleMonth,@Title=@Title",

                                            new SqlParameter("DoctorId", objMyScheduleModel.DoctorId),
                                            new SqlParameter("ScheduleDate", objMyScheduleModel.PresentDate),
                                            new SqlParameter("ScheduleDay", type),
                                             new SqlParameter("ScheduleStartShift", objMyScheduleModel.ScheduleStartShift),
                                            new SqlParameter("ScheduleEndShift", objMyScheduleModel.ScheduleEndShift),
                                            new SqlParameter("IsWorking", objMyScheduleModel.IsWorking),
                                            new SqlParameter("ScheduleMonth", month),
                                             new SqlParameter("Title", objMyScheduleModel.Title));
                    }
                    else
                    {
                        //he is not working ...do not save the value
                    }
                }
                else
                {
                    DateTime? startShift = null;
                    DateTime? EndShift = null;
                    string tit = null;

                    //if value =2 then it will be not working day for Doctor:
                    int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("SaveDoctorsTimeSchedule @DoctorId=@DoctorId,@ScheduleDate=@ScheduleDate,@ScheduleDay=@ScheduleDay,@ScheduleStartShift=@ScheduleStartShift,@ScheduleEndShift=@ScheduleEndShift,@IsWorking=@IsWorking,@ScheduleMonth=@ScheduleMonth,@Title=@Title",

                                            new SqlParameter("DoctorId", objMyScheduleModel.DoctorId),
                                            new SqlParameter("ScheduleDate", objMyScheduleModel.PresentDate),
                                            new SqlParameter("ScheduleDay", type),
                                           //new SqlParameter("ScheduleStartShift", (object)objMyScheduleModel.ScheduleStartShift ?? DBNull.Value),
                                            new SqlParameter("ScheduleStartShift", (object)startShift ?? DBNull.Value),
                                            new SqlParameter("ScheduleEndShift", (object)EndShift ?? DBNull.Value),
                                            new SqlParameter("IsWorking", false),
                                            new SqlParameter("ScheduleMonth", month),
                                            new SqlParameter("Title", tit));


                }
            }
            catch (Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return View();
        }             


        [HttpGet]
        public JsonResult ShowShiftTimings()
        {
            ShowTimeModel model = new ShowTimeModel();
            model.DoctorId = ApplicationSession.UserId;
            try
            {
                
                String sDate = System.DateTime.Now.ToString();
                DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

                String dy = datevalue.Day.ToString();
                string month = datevalue.ToString("MMMM");
                
               
               List<ShowTimeModel> calenderData = _objHealthCareDBContext.Database.SqlQuery<ShowTimeModel>("ShowTimeslot @DoctorId=@DoctorId,@ScheduleMonth=@ScheduleMonth",
                                                                                                new SqlParameter("DoctorId", model.DoctorId),
                                                                                               new SqlParameter("ScheduleMonth", month)).ToList();
                foreach (var item in calenderData)
                {
                  

                
                    DateTime stime = DateTime.Today.Add(item.ScheduleStartShift);
                    string startTime = stime.ToString("hh:mm tt").ToLower();
                    DateTime etime = DateTime.Today.Add(item.ScheduleEndShift);
                    string endTime = etime.ToString("hh:mm tt").ToLower();
                    item.value = startTime + '-' + endTime;



                    //item.ScheduleStartShift = TimeSpan.Parse(a);
                    //item.value = time.ToString("hh:mm tt").ToLower();
                    DateTime startval = Convert.ToDateTime(item.start);
                    DateTime endval = Convert.ToDateTime(item.start);
                    item.start = startval.ToString("MM/dd/yyyy hh:mm tt").ToLower();
                    item.end = endval.ToString("MM/dd/yyyy hh:mm tt").ToLower();
                }
                return Json(calenderData, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { Exception = ex.Message }, JsonRequestBehavior.AllowGet);
            }            
        }
            
  }
    
}
