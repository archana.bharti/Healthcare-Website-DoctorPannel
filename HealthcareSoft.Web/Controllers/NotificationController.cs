﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    public class NotificationController : Controller
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();

        [HttpGet]
        public ActionResult GetNotifications()
        {
            
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            if (TempData["IsCompleted"] != null)
            {
                ViewBag.IsCompleted = TempData["IsCompleted"].ToString();
            }
            var DoctorId = ApplicationSession.UserId;
            List<DataSubmitModel> lstDataSubmit = new List<DataSubmitModel>();

            var model = _objHealthCareDBContext.Database.SqlQuery<NotificationModel>("GetNotification @DoctorId=@DoctorId",
                new SqlParameter("@DoctorId", DoctorId)).ToList();

            NotificationSummaryModel notification = new NotificationSummaryModel()
            {
                count = model.Count(),
                NotificationList = model 
            };

            foreach(var item in model)
            {
                    if(item.ProfilePicture ==null)
                    {
                         item.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                    }
                    else if(item.ProfilePicture != null)
                    {
                        item.ProfilePicture = "http://18.219.253.188:8081" + item.ProfilePicture;
                    }
            }
            return View(model);            
        }

        [HttpGet]
        public JsonResult GetNotificationsLive()
        {
            var DoctorId = ApplicationSession.UserId;
            List<DataSubmitModel> lstDataSubmit = new List<DataSubmitModel>();

            var model = _objHealthCareDBContext.Database.SqlQuery<NotificationModel>("GetNotification @DoctorId=@DoctorId",
                new SqlParameter("@DoctorId", DoctorId)).ToList();


            foreach (var item in model)
            {
                if (item.ProfilePicture == null)
                {
                    item.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                }
                else if (item.ProfilePicture != null)
                {
                    item.ProfilePicture = "http://18.219.253.188:8081" + item.ProfilePicture;
                }
            }

            NotificationSummaryModel notification = new NotificationSummaryModel()
            {
                count = model.Count(),
                NotificationList = model
            };
            return Json(data: notification, behavior: JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public ActionResult CancelAppointment(Int64 id)
        {
            try
            {

                int rowCancelAppointment = _objHealthCareDBContext.Database.ExecuteSqlCommand("Update Appointment set IsActive=0,ReasonForVisit='Cancel'  where Id={0}", id);

                if (rowCancelAppointment > 0)
                {
                    TempData["Message"] = "Appointment Deleted Successfully!";
                    TempData.Keep();
                    return RedirectToAction("GetNotifications", "Notification");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }



        [HttpPost]
        public ActionResult CancelAppointmentPopUp(string id)
        {
            try
            {

                int rowCancelAppointment = _objHealthCareDBContext.Database.ExecuteSqlCommand("Update Appointment set IsActive=0,ReasonForVisit='Cancel'  where Id={0}", id);

                if (rowCancelAppointment > 0)
                {
                    TempData["Message"] = "Appointment Deleted Successfully!";
                    TempData.Keep();
                    return RedirectToAction("GetNotifications", "Notification");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return View();
        }

        [HttpGet]
        public ActionResult CompleteAppointment(Int64 id)
        {
            try
            {

                int rowCancelAppointment = _objHealthCareDBContext.Database.ExecuteSqlCommand("Update Appointment set IsCompleted=1 where Id={0}", id);

                if (rowCancelAppointment > 0)
                {
                    TempData["IsCompleted"] = "Appointment Completed Successfully!";
                    TempData.Keep();
                    return RedirectToAction("GetNotifications", "Notification");
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }
    }
}
