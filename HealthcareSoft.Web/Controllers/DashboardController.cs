﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.IRepository;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Data.Repository;
using HealthcareSoft.Web.CustomFilters;
using HealthcareSoft.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    [CheckAuthentication]
    public class DashboardController : Controller
    {
        IDashboardRepository _IDashboardRepository;
        public DashboardController()
        {
            _IDashboardRepository = new DashboardRepository();
        }
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();
       

        public ActionResult Index()
        {
            try
            {
                if (TempData["Success"] != null)
                {
                    ViewBag.Success = TempData["Success"].ToString();
                }

                if (TempData["File"] != null)
                {
                    ViewBag.File = TempData["File"].ToString();
                }

                if (TempData["Message"] != null)
                {
                    ViewBag.Message = TempData["Message"].ToString();
                }              

                TodaysAppointmentModel model = new TodaysAppointmentModel();
                model.DoctorId = ApplicationSession.UserId;
            

                var pateintslist = _IDashboardRepository.TodaysAppoinmentList(model.DoctorId);
                return View(pateintslist);
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }            
        }      
    }
}
