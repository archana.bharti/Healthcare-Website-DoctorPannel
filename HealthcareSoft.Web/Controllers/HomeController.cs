﻿using HealthcareSoft.Data;
using HealthcareSoft.Data.Model;
using HealthcareSoft.Web.CustomFilters;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareSoft.Web.Controllers
{
    public class HomeController : Controller
    {
        private HealthCareDBContext _objHealthCareDBContext = new HealthCareDBContext();


        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                var data = _objHealthCareDBContext.Database.SqlQuery<ContentManagementModel>("GetSliderContect").ToList();
                return View(data);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult Index(ContactUsModel objContactUsModel)
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactUsModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int rowEffected = _objHealthCareDBContext.Database.ExecuteSqlCommand("SaveContactUsQuerry @Name=@Name,@Email=@Email,@PhoneNo=@PhoneNo,@Description=@Description",
                                new SqlParameter("Name", model.Name),
                                new SqlParameter("Email", model.Email),
                                 new SqlParameter("PhoneNo", model.PhoneNo),
                                  new SqlParameter("Description", model.Description));
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                ModelState.AddModelError("model", "model is not valid");
            }

            return View();
        }

        public ActionResult HowItWorks()
        {
            return View();
        }
    }
}