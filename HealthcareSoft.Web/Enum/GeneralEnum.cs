﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareSoft.Web.Enum
{
    public class GeneralEnum
    {
        public enum ReportType
        {
            Report = 1,
            ClinicalReport = 2
        }
    }
}