﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareSoft.Web.Models
{
    public class NotificationSummaryModel
    {
        public int count { get; set; }
        public List<NotificationModel> NotificationList { get; set; }
    }
}