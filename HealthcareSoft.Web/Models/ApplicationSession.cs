﻿using HealthcareSoft.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareSoft.Web.Models
{
    [Serializable()]
    public class ApplicationSession
    {
        private static string key = "Login";


        public static Int64 UserId { get; set; }
        public static string UserName { get; set; }
        public static string HospitalName { get; set; }
        public static string QualificationName { get; set; }
        public static string DepartmentName { get; set; }


        public static void Login(LoginModel objUser)
        {

            System.Web.HttpContext.Current.Session[key] = objUser;
            System.Web.HttpContext.Current.Session.Timeout = 60;
            UserName = objUser.FirstName;
            UserId = objUser.Id;
            HospitalName = objUser.HospitalName;
            QualificationName = objUser.QualificationName;
            DepartmentName = objUser.DepartmentName;
        }


        public static LoginModel CurrentUser
        {
            get
            {
                if ((System.Web.HttpContext.Current.Session != null))
                {
                    return (LoginModel)System.Web.HttpContext.Current.Session[key];
                }
                else
                {
                    return null;
                }
            }
        }
        public static void Logout()
        {
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
        }
    }
}